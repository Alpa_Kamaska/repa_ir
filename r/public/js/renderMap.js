    function initMap() {
      const componentForm = [
        'location'
        
      ];
      const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 17,
        center: { lat: 19.4978, lng: -99.1269 },
        mapTypeControl: false,
        fullscreenControl: true,
        zoomControl: true,
        streetViewControl: true
      });
      const marker = new google.maps.Marker({map: map, draggable: false});
      const autocompleteInput = document.getElementById('location');
      const autocomplete = new google.maps.places.Autocomplete(autocompleteInput, {
        fields: ["address_components", "geometry", "name"],
        types: ["address"],
      });
      autocomplete.addListener('place_changed', function () {
        marker.setVisible(false);
        const place = autocomplete.getPlace();
        
        if (!place.geometry) {
         
          window.alert('No es una direccion : \'' + place.name + '\'');
          return;
        }
        
        renderAddress(place);
        fillInAddress(place);
      });

      function fillInAddress(place) {
         console.log(place);
        const addressNameFormat = {
          'street_number': 'short_name',
          'route': 'long_name',
          'locality': 'long_name',
          'country': 'long_name',
          
       
        };

        const getAddressComp = function (type) {
          for (const component of place.address_components) {
            if (component.types[0] === type) {
              return component[addressNameFormat[type]];
            }
          }
          return '';
        };
        document.getElementById('location').value = getAddressComp('street_number') + ' '
                  + getAddressComp('route');
        for (const component of componentForm) {
          if (component !== 'location') {
            document.getElementById(component).value = getAddressComp(component);
          }
        }
      }
      function renderAddress(place) {
        map.setCenter(place.geometry.location);
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
      }
    }