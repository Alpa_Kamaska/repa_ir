const staticRepairDocs = "static-repair";
const assets = [ 
"/",
"index.html",
"/css/materialize.min.css",
"/css/style.css",
"/js/materialize.min.js",
"/js/script.js"
]
self.addEventListener("install", installEvent =>{
   installEvent.waitUntil(
      caches.open(staticRepairDocs).then(cache => {
         cache.addAll(assets);
         console.log('carga de elementos... ok');
      })
      )
});

self.addEventListener("fetch", fetchEvent=>{
    fetchEvent.respondWith(
	caches.match(fetchEvent.request).then(res =>{
	    return res || fetch(fetchEvent.request)
       console.log(res);
	})
    );
});
